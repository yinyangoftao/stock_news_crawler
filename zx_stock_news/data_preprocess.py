import re
import os
import csv
import json
import nltk
nltk.download('punkt')
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem.wordnet import WordNetLemmatizer
#!/usr/bin/env python3
import json
from math import log
import datetime as dt
from datetime import datetime
import ipdb
import tqdm
from tqdm import tqdm
import torch
import torch.nn.functional as F
import pickle

# relative return label idea credit: https://github.com/WayneDW/Sentiment-Analysis-in-Event-Driven-Stock-Price-Movement-Prediction
# but we use different setting here(price after news - price before news)

def closest_trd_date_prc(baseDate):
	wkday = baseDate.weekday()
	# if news happened on trading day
	if wkday != 5 and wkday != 6:
		# TODO or should we use open price tomorrow?
		date = datetime.strftime(baseDate, "%Y-%m-%d")
		prev_date, prev_prc = date, 'open'
		adhoc_date, adhoc_prc = date, 'close'
	# if news happened on non-trading day
	else:
		prevd = (baseDate - datetime.timedelta(days=wkday - 4)).strftime("%Y-%m-%d")	
		nextd = (baseDate + datetime.timedelta(days=7 - wkday)).strftime("%Y-%m-%d")
		prev_date, prev_prc = prevd, 'close'
		adhoc_date, adhoc_prc = nextd, 'open'
	return prev_date, prev_prc, adhoc_date, adhoc_prc

def calc_mid_long_return(ticker, date, delta, priceSet):
	baseDate = datetime.strptime(date, "%Y-%m-%d")
	# wkday = baseDate.weekday()
	nextDate = (baseDate + dt.timedelta(days=delta))
	try:
		if delta == 1:
			# if news happened on trading day
			prev_date, prev_prc, adhoc_date, adhoc_prc = closest_trd_date_prc(baseDate)
			return_self = log(priceSet[ticker][adhoc_prc][adhoc_date]) - log(priceSet[ticker][prev_prc][prev_date])
			return_sp500 = log(priceSet['^GSPC'][adhoc_prc][adhoc_date]) - log(priceSet['^GSPC'][prev_prc][prev_date])
		else:
			prev_date, _, _, _ = closest_trd_date_prc(baseDate)
			adhoc_date, _, _, _ = closest_trd_date_prc(nextDate)
			return_self = log(priceSet[ticker]['close'][adhoc_date]) - log(priceSet[ticker]['close'][prev_date])
			return_sp500 = log(priceSet['^GSPC']['close'][adhoc_date]) - log(priceSet['^GSPC']['close'][prev_date])
		return True, round(return_self, 4), round(return_self - return_sp500, 4) # relative return
	except:
		return False, 0, 0

def extract_price_info(data_folder, ticker):
	"""
	rtype:dict, length-3 list for each date 
	"""
	return_fn = os.path.join(data_folder, ticker, "return_close_open.csv")
	if os.path.exists(return_fn):
		print("loading from ",return_fn)
		returns = {}
		return_rdr = csv.DictReader(open(return_fn))
		for l in return_rdr:
			date, absret_short,absret_mid,absret_long, short, mid, long_ret = l['date'], float(l['ashort']),float(l['amid']), float(l['along']), float(l['rshort']), float(l['rmid']), float(l['rlong'])
			returns[date] = [absret_short,absret_mid,absret_long,short,mid,long_ret]
		return returns
	print("creating return from ",return_fn)
	price_fn = os.path.join(data_folder, 'prices', ticker+'.csv')
	sp_fn = os.path.join(data_folder, 'prices', '^GSPC.csv')
	ticker_rdr = csv.DictReader(open(price_fn))
	sp_rdr = csv.DictReader(open(sp_fn))
	priceSet = {'^GSPC':{'close':{}, 'open':{}}, ticker:{'close':{}, 'open':{}}}
	for reader, tkr in [(ticker_rdr, ticker),(sp_rdr, '^GSPC')]:
		for l in reader:
			date = l['Date']
			openp = float(l['Open'])
			adjp = float(l['Close'])
			priceSet[tkr]['open'][date] = openp
			priceSet[tkr]['close'][date] = adjp

	dateSet = priceSet['^GSPC']['close'].keys()
	returns = {} # 1-depth dictionary

	with open(return_fn, 'w') as f:
		return_wrt = csv.DictWriter(f, fieldnames = ['date','ashort','amid','along','rshort','rmid','rlong'])
		return_wrt.writeheader()
		for day in dateSet:
			date = datetime.strptime(day, "%Y-%m-%d").strftime("%m%d%Y") # change date 2014-01-01 to 20140101
			tag_short, absret_short, return_short = calc_mid_long_return(ticker, day, 1, priceSet)
			tag_mid, absret_mid, return_mid = calc_mid_long_return(ticker, day, 7, priceSet)
			tag_long, absret_long, return_long = calc_mid_long_return(ticker, day, 28, priceSet)
			# TODO process respectively
			if tag_short and tag_mid and tag_long:
				returns[date] = [absret_short,absret_mid,absret_long,return_short,return_mid,return_long]
				return_wrt.writerow({'date':date,'ashort':absret_short,'amid':absret_mid,'along':absret_long,'rshort':return_short,'rmid':return_mid,'rlong':return_long})
	return returns

stop_words_set = set(stopwords.words('english'))

def nvg_domain(url):
	return url.split('//')[-1].split('/')[0]

def unify_word(word):  # went -> go, apples -> apple, BIG -> big
	"""unify verb tense and noun singular"""
	ADJ, ADJ_SAT, ADV, NOUN, VERB = 'a', 's', 'r', 'n', 'v'
	for wt in [ADJ, ADJ_SAT, ADV, NOUN, VERB]:
		try:
			word = WordNetLemmatizer().lemmatize(word, pos=wt)
		except:
			pass
	return word.lower()

class Tokenizer():
	"""
	sentences: list(str) iterator
	output: list(list(str)) iterator
	"""
	# break article/review into sentences and then break into words
	def __init__(self, w2id=None, max_sent_len=-1, max_word_len=-1,delimiters = None, pattern = None):
		assert w2id, "No dictionnary to tokenize text \n set the word embedding plan first. \n"
		self.w2id = w2id
		self.max_sent_len = max_sent_len
		self.max_word_len = max_word_len
		delimiters=(';', '\n', '.') if delimiters is None else delimiters
		self.delimiters = [re.escape(x) for x in delimiters] 
		self.pattern = re.compile("|".join(self.delimiters)) if pattern is None else pattern
		if self.max_sent_len < 0 and self.max_word_len < 0:
			self.trim = False

	def transform(self,article):
		"""
		article: 2-layer list
		list of review, review is a list of sequences, sequences is a list of int
		"""
		tok_a = []
		sentences = [s.split() for s in re.split(self.pattern, article) if len(s) > 0]
		for j,sent in enumerate(sentences):  
			if self.trim and j>= self.max_sent_len:
				break
			s = []
			for k,word in enumerate(sent):
				if self.trim and k >= self.max_word_len:
					break
				s.append(self.w2id.get(unify_word(word), self.w2id["_unk_"]))

			if len(s) >= 1:
				tok_a.append(s)
		if len(tok_a) == 0:
			tok_a = [[self.w2id["_unk_"]]]
		return tok_a

def filter_valid_raw(load_path, save_path):
	"""
	from the non-empty body data, select those with long enough body and within the 
	valid date ranges, 1-time function, only useful in the designing stage
	load_path: a folder
	save_path: a json file
	"""
	res = []
	with open(load_path) as f, open(save_path,"w") as sf:
		data = json.load(f)
		for rec in data:
			if len(rec['body']) > 50:
				res += rec,
		json.dump(res,sf)

def tokenize_corpus_and_price(data_folder, ticker, w2id, token_fn, raw_fn = "yahoo_long_body_raw.json"):
	price_folder = os.path.join(data_folder, "prices")
	ticker_folder = os.path.join(data_folder, ticker)
	# prepare price info
	ret_by_date = extract_price_info(data_folder, ticker)
	# prepare valid text
	raw_path = os.path.join(ticker_folder,raw_fn)
	if os.path.exists(raw_path):
		with open(raw_path) as rf:
			valid_raw_corpus = json.load(rf)
	else:
		prefixed = [fn for fn in os.listdir(ticker_folder) if fn.startswith("detail")]
		all_data = {}
		for fl in prefixed:
			data = json.load(open(os.path.join(ticker_folder,fl)))
			# merge corpus by date
			for datestr, val in data.items():
				if not datestr in all_data:
					all_data[datestr] = []
				all_data[datestr] += val
				# need to further deal with repeating ends
		dobjs = list(map(lambda s: datetime.strptime(s, "%m%d%Y"), all_data.keys()))
		dstrs = list(map(lambda s: datetime.strftime(s, "%m%d%Y"), sorted(dobjs)))
		valid_raw_corpus = []
		for dstr in tqdm(dstrs):
				if not dstr in ret_by_date:
					continue
				for rec in all_data[dstr]:
					try:
						if nvg_domain(rec['url']) == 'finance.yahoo.com':
							# ipdb.set_trace()
							title = rec['title']#corpus_tokenize(rec['title'])
							summary = rec['summary']#corpus_tokenize(rec['summary'])
							body = rec['body']#corpus_tokenize(rec['body'])
							if len(body) < 50:
								continue
							ashort,amid,along,rshort, rmid, rlong = ret_by_date[dstr]
							valid_raw_corpus += {'title':title,'body':body,'summary':summary,'ticker':ticker,'date':dstr},
					except:
						# ipdb.set_trace()
						print("wrong record for ticker{}:".format(ticker), rec)
		with open(raw_path,"w") as f:
			json.dump(valid_raw_corpus, f)
 
	# tokenizing and dump to token_fn
	# ipdb.set_trace()
	toker = Tokenizer(w2id)
	body = []
	title = []
	summary = []
	dstrs = []
	ashort,amid,along,rshort, rmid, rlong = [],[],[],[],[],[]
	for rec in tqdm(valid_raw_corpus):
		body += toker.transform(rec['body']),
		title += toker.transform(rec['title']),
		summary += toker.transform(rec['summary']),
		dstr = rec['date']
		dstrs += dstr,
		ash, am, al, rsh, rm, rl = ret_by_date[dstr]
		ashort += ash,
		amid += am,
		along += al,
		rshort += rsh,
		rmid += rm,
		rlong += rl,
	tokens = {'title':title,'body':body,'summary':summary,'ashort':ashort,'amid':amid,'along':along,'rshort':rshort,'rmid':rmid,'rlong':rlong,'ticker':ticker,'date':dstrs}
	#save tokens
	# ipdb.set_trace()
	token_path = os.path.join(ticker_folder,token_fn)
	with open(token_path, 'wb') as wf:
		pickle.dump(tokens, wf)
	return tokens

def load_ebd_glove(filepath, all_possible_words2id = None, offset = 2):
	"""
	param:  all_possible_words2id: word 2 id dict, id from 0 to len(.)
	return: id2vec:actually a tensor, embedding for each token
	        w2id: word(token) 2 id of the token
	"""
	w2id = {'_unk_':1, '_pad_':0}
	cnt = offset
	id2vec = []
	save_vecs, save_words, unk_or_pad = [], [], ['<unk>', 'unk', 'UNK', '_unk_', '_pad_']
	with open(filepath, 'r', encoding='utf-8') as f:
		for line in f:
			tuples = line.split()
			word, embed = tuples[0], torch.tensor([float(embed_col) for embed_col in tuples[1:]])
			if len(embed) < 25:
				# we hit whitespace
				print("word length wrong!",word,len(embed))
				continue
			if word in unk_or_pad:  # unify UNK
				continue
			# if we have a possible word list
			if all_possible_words2id:
				if not word in all_possible_words2id:
					continue
				word_id = all_possible_words2id[word]
				# we don't want some word other than UNK to occupy the 0th location
				if word_id in [0,1] and not word in unk_or_pad:
					save_vecs += embed,
					save_words += word,
				w2id[word] = word_id
			else:
				w2id[word] = cnt
				cnt += 1
			id2vec += embed,
	cnt = len(w2id)
	for word, vec in zip(save_words, save_vecs):
		w2id[word] = cnt
		id2vec[cnt] = vec
		cnt += 1
	id2vec = torch.stack(id2vec)
	#vectors for unknown and padding are the same
	id2vec = F.pad(id2vec, (0,0,offset,0))
	print("loading dictionary:", len(w2id) , "words")
	# id2w = {v:k for k,v in w2id.items()}
	return id2vec, w2id

if __name__ == "__main__":
	# load_path = "/mnt/hhd/data/stock_crawl_zx/intrinio/BIDU/yahoo_raw.json"
	# save_path = "/mnt/hhd/data/stock_crawl_zx/intrinio/BIDU/yahoo_long_body_raw0.json"
	# filter_valid_raw(load_path, save_path)
	id2vec, w2id  = load_ebd_glove('/mnt/hhd/data/glove_twitter_pre/glove.twitter.27B.25d.txt')
	tokenize_corpus_and_price("/mnt/hhd/data/stock_crawl_zx/intrinio/","BIDU", w2id, "yahoo_token.pkl", raw_fn = "yahoo_long_body_raw.json")