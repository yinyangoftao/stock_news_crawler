import argparse
import matplotlib.pyplot as plt
import pickle
import ipdb
import numpy as np
import csv
import os
import datetime as dt
from datetime import datetime
# from main import get_metrics

def daterange(start, end):
	"""
	rtype:list[str], date from start to end
	"""
	if type(start) == str:
		start = datetime.strptime(start,'%m%d%Y')
		end = datetime.strptime(end,'%m%d%Y')
	numdays = (end-start).days
	date_list = [end - dt.timedelta(days=x) for x in range(0, numdays)]
	datestrs = list(map(lambda x:x.strftime("%m%d%Y"), date_list))
	return datestrs

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='NLP model for stock news')
	parser.add_argument('-mp','--model_folder',type=str, default='/mnt/hhd/check_points/nlp4stock/20181230_171950',help='check point path')
	parser.add_argument('-dp','--data_folder',type=str, default='/mnt/hhd/data/stock_crawl_zx/intrinio/',help='raw news path')
	args = parser.parse_args()
	votes = pickle.load(open(args.model_folder+'/votes','rb'))
	probs = pickle.load(open(args.model_folder+'/probs','rb'))
	ground_truth = pickle.load(open(args.model_folder+'/ground_truth','rb'))
	
	# ipdb.set_trace()
	

	for ticker in votes:
		# for the correctly predicted examples, sum of prob of the predicted class vs other class
		correct_pred = []
		correct_other = []
		false_pred = []
		false_other = []
		# false_by_date = []
		gt_by_date = []
		vt = votes[ticker]
		gt = ground_truth[ticker]
		prob = probs[ticker]
		openp, closep = [],[]
		#load price info
		dateobjs = [datetime.strptime(dstr,'%m%d%Y') for dstr in sorted(gt.keys())]
		mindobj, maxdobj = min(dateobjs), max(dateobjs)		
		price_fn = os.path.join(args.data_folder, 'prices', ticker+'.csv')
		sp_fn = os.path.join(args.data_folder, 'prices', '^GSPC.csv')
		ticker_rdr = csv.DictReader(open(price_fn))
		sp_rdr = csv.DictReader(open(sp_fn))
		tickers_and_sp = list(votes.keys())+['^GSPC']
		priceSet = {tkr:{'close':{}, 'open':{}} for tkr in tickers_and_sp}
		
		for reader, tkr in [(ticker_rdr, ticker),(sp_rdr, '^GSPC')]:
			for l in reader:
				date = l['Date']
				date = date[5:7] + date[8:] + date[:4]
				priceSet[tkr]['open'][date] = float(l['Open'])
				priceSet[tkr]['close'][date] = float(l['Close'])
		# ipdb.set_trace()
		for dstr in daterange(mindobj, maxdobj):
			if dstr in priceSet[ticker]['open']:
				openp += priceSet[ticker]['open'][dstr],
				closep += priceSet[ticker]['close'][dstr],
			else:
				openp += 0,
				closep += 0,
			if dstr in gt:
				gtd = gt[dstr]
				gt_by_date += gtd,
				vote_today = vt[dstr]
				crc_cnt = 0 if not gtd in vote_today else vote_today[gtd]
				vote_today.pop(gtd,-1)
				fls_cnt = sum(vote_today.values())
				# correct_by_date += crc_cnt,
				# false_by_date += fls_cnt,
				correct_pred += crc_cnt*prob[dstr]['crc'][0],
				correct_other += crc_cnt*prob[dstr]['crc'][1],
				false_pred += fls_cnt*prob[dstr]['fls'][0],
				false_other += fls_cnt*prob[dstr]['fls'][1],
			else:
				correct_pred += 0,
				correct_other += 0,
				false_pred += 0,
				false_other += 0,
		fig, ax = plt.subplots()

		openp, closep = np.array(openp), np.array(closep)
		# openp += max(correct_pred+correct_other+false_pred+false_other) + 1
		# closep += max(correct_pred+correct_other+false_pred+false_other) + 1

		
		ind = np.arange(len(correct_pred))    # the x locations for the groups
		
		wkdy = [datetime.strptime(dstr,'%m%d%Y').weekday() for dstr in daterange(mindobj, maxdobj)]    # the x locations for the groups
		
		width = 0.35         # the width of the bars
		p1 = ax.bar(ind, correct_pred, width, color='g', bottom=0)
		p2 = ax.bar(ind, correct_other, width, color='r', bottom=correct_pred)
		p3 = ax.bar(ind + width, false_pred, width, color='b', bottom=0)
		p4 = ax.bar(ind + width, false_other, width, color='y', bottom=false_pred)

		ax.set_title('stats for '+ ticker)
		ax.set_xticks(ind + width / 2)
		ax.set_xticklabels(wkdy)

		ax.legend((p1[0], p2[0], p3[0], p4[0]), ('crc.pred', 'crc.other', 'fls.pred', 'fls.other'))

		
		ax2 = ax.twinx()
		ax2.plot(ind, openp, 'r.')
		ax2.plot(ind, closep, 'g.')
		fig.tight_layout()
		# plt.show()
		
		# manager = plt.get_current_fig_manager()
		# manager.window.showMaximized()

		plt.savefig(args.model_folder+'/'+ticker, bbox_inches='tight',dpi = 300)
		plt.close(fig)
		crc_pred_sum = sum(correct_pred)
		crc_other_sum = sum(correct_other)
		crc_all = crc_pred_sum+crc_other_sum
		fls_pred_sum = sum(false_pred)
		fls_other_sum = sum(false_other)
		fls_all = fls_pred_sum+fls_other_sum
		print('{} avg correct pred v.s. other {}:{}, false pred v.s. other {}:{}'.format(ticker, crc_pred_sum/crc_all, crc_other_sum/crc_all, fls_pred_sum/fls_all, fls_other_sum/fls_all))
		# print('by article:', sum(correct_by_date)/sum(correct_by_date+false_by_date))
		# print('by date:', sum(crc > fls for crc, fls in zip(correct_by_date, false_by_date))/len(correct_by_date))