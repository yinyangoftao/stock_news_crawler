#!/usr/bin/env bash
dataset="imdb"
# max_feats=100000
max_sents=8
max_words=32
embedding_size=25

model_folder="/mnt/hhd/check_points/nlp4stock"
embedding_path="/mnt/hhd/data/glove_twitter_pre/glove.twitter.27B.25d.txt"

solver_type="adam"
batch_size="32"
epochs=100
lr=0.0001
lr_halve_interval=-1
gamma=0.9
snapshot_interval=10
gpuid=0

python main.py --model_folder ${model_folder} \
                --max_sents ${max_sents} \
                --max_words ${max_words} \
                --solver_type ${solver_type} \
                --batch_size ${batch_size} \
                --epochs ${epochs} \
                --lr ${lr} \
                --lr_halve_interval ${lr_halve_interval} \
                --gamma ${gamma} \
                --snapshot_interval ${snapshot_interval} \
                --gpuid ${gpuid} \
                --embedding_size ${embedding_size}