import argparse
import dataset
import datetime as dt
from datetime import datetime
from sklearn import utils, metrics
from dataset import YahooFin, tuple_batch
from torch.utils.data import DataLoader, Dataset
from data_preprocess import load_ebd_glove
import matplotlib.pyplot as plt
import torch.nn.functional as F
import numpy as np
import ipdb
import os
import torch
from net import HAN
import logging
import tqdm
from tqdm import tqdm
from collections import Counter
# from collections import default
import pickle

def get_logger(logdir, logname, loglevel=logging.INFO):
    logger = logging.getLogger()
    logger.setLevel(loglevel)

    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

    fh = logging.FileHandler(os.path.join(logdir, logname))
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    # ch.setFormatter(formatter)
    logger.addHandler(ch)

    return logger

def get_metrics(cm, list_metrics):
    """Compute metrics from a confusion matrix (cm)
    cm: sklearn confusion matrix
    returns:
    dict: {metric_name: score}

    """
    dic_metrics = {}
    total = np.sum(cm)

    if 'accuracy' in list_metrics:
        out = np.sum(np.diag(cm))
        dic_metrics['accuracy'] = out/total

    if 'pres_0' in list_metrics:
        num = cm[0, 0]
        den = cm[:, 0].sum()
        dic_metrics['pres_0'] =  num/den if den > 0 else 0

    if 'pres_1' in list_metrics:
        num = cm[1, 1]
        den = cm[:, 1].sum()
        dic_metrics['pres_1'] = num/den if den > 0 else 0

    if 'recall_0' in list_metrics:
        num = cm[0, 0]
        den = cm[0, :].sum()
        dic_metrics['recall_0'] = num/den if den > 0 else 0

    if 'recall_1' in list_metrics:
        num = cm[1, 1]
        den = cm[1, :].sum()
        dic_metrics['recall_1'] =  num/den if den > 0 else 0

    return dic_metrics

def save(net,dic,path):
    """
    Saves a model's state and it's embedding dic by piggybacking torch's save function
    """
    dict_m = net.state_dict()
    dict_m["word_dic"] = dic    
    torch.save(dict_m,path)

def create_model(args, logger, n_tokens, tensor=None):
    logger.info("Creating model...")
    if args.model_weights_path and os.path.exists(args.model_weights_path):
        logger.info(" --loading existing weights from: {}".format(args.model_weights_path))
        state = torch.load(args.model_weights_path)
        wdict = state["word_dic"]
        n_tokens = len(wdict)

        net = HAN(ntoken=len(state["word_dic"]),emb_size=state["embed.weight"].size(1),hid_size=state["sent.rnn.weight_hh_l0"].size(1),num_class=state["lin_out.weight"].size(0))
        del state["word_dic"]
        net.load_state_dict(state)

    else:
        net = HAN(n_tokens, args.n_classes, emb_size=args.embedding_size, hid_size=50)

    if args.embedding_plan:
        net.set_emb_tensor(tensor)
    return net

def draw_fig(stats,sav_path,symbols = ['.','b--','r--','g:','y:','m-.','c-.']):
	# ipdb.set_trace()
	L = set(map(len, stats.values()))
	assert len(L) == 1, "need same # of epochs for all metrics"
	L = L.pop()

	x = list(range(L))
	fig, ax = plt.subplots()
	for i,ky in enumerate(stats):
		fig_tmp, ax_tmp = plt.subplots()
		y = stats[ky]
		ax_tmp.plot(x, y, label=ky)
		fig_tmp.savefig(sav_path+'_'+ky)
		plt.close(fig_tmp)
		ax.plot(x, y, symbols[i],label=ky)
	ax.legend()
	plt.savefig(sav_path)

def train_base_model(args, net, wdict, tr_loader, te_loader, logger, list_metrics=['accuracy', 'pres_0', 'pres_1', 'recall_0', 'recall_1']):
	if args.solver_type == 'sgd':
		optimizer = torch.optim.SGD(net.parameters(), lr = args.lr)    
	elif args.solver_type == 'adam':
		optimizer = torch.optim.Adam(net.parameters(), lr = args.lr)

	scheduler = None
	if args.lr_halve_interval > 0:
		scheduler = torch.optim.lr_scheduler.StepLR(optimizer, args.lr_halve_interval, gamma=args.gamma, last_epoch=-1)
	
	trn_accum_metrics = {ky:[] for ky in list_metrics+['logloss','lr']}
	tst_accum_metrics = {ky:[] for ky in list_metrics}
	# ipdb.set_trace()
	for epoch in range(1, args.epochs + 1):
		train_tmp_metrics = train(epoch,net, tr_loader, device,logger, msg="training", optimize=True, optimizer=optimizer, scheduler=scheduler, criterion=criterion,list_metrics=list_metrics)
		test_tmp_metrics = train(epoch,net, te_loader, device,logger, msg="testing ",list_metrics=list_metrics)

		for ky in trn_accum_metrics.keys():
			trn_accum_metrics[ky] += train_tmp_metrics[ky][-1],
		for ky in tst_accum_metrics.keys():
			tst_accum_metrics[ky] += test_tmp_metrics[ky][-1],

		draw_fig(trn_accum_metrics,args.model_folder+"/train")
		draw_fig(tst_accum_metrics,args.model_folder+"/test")

		if (epoch % args.snapshot_interval == 0) and (epoch > 0):
			path = "{}/model_epoch_{}".format(args.model_folder,epoch)
			print("snapshot of model saved as {}".format(path))
			save(net, wdict, path=path)

	path = "{}/model_epoch_{}".format(args.model_folder,args.epochs)
	print("snapshot of model saved as {}".format(path))
	save(net, wdict, path=path)
	return net

def train(epoch,net,data_loader,device,logger, msg="val/test",optimize=False,optimizer=None,scheduler=None,criterion=None, list_metrics=['accuracy', 'pres_0', 'pres_1', 'recall_0', 'recall_1'], rettype = 'mtrc_by_date'):

	net.train() if optimize else net.eval()

	epoch_loss = 0
	nclasses = len(list(net.parameters())[-1])
	# cm = np.zeros((nclasses,nclasses), dtype=int)

	probs = {}
	if optimize:
		list_metrics += ['logloss','lr']
	majority_vote = {}
	date_true = {}
	acc_prob_by_date = {}
	with tqdm(total=len(data_loader),desc="Epoch {} - {}".format(epoch, msg)) as pbar:
		for iteration, (batch_t,r_t,sent_order,ls,lr,review,datestr) in enumerate(data_loader):

			data = (batch_t,r_t,sent_order)
			data = [x.to(device) for x in data]

			if optimize:
				optimizer.zero_grad()

			out = net(data[0],data[2],ls,lr)
			ty_prob = F.softmax(out, 1) # probabilites

			#metrics
			y_true = r_t.detach().cpu().numpy()
			y_pred = ty_prob.max(1)[1]
			yprednp = y_pred.detach().cpu().numpy()
			prob_np = ty_prob.detach().cpu().numpy()
			# ipdb.set_trace()
			for i,dstr in enumerate(datestr):
				date_true[dstr] = y_true[i]
				if not dstr in majority_vote:
					majority_vote[dstr] = Counter()
				majority_vote[dstr][yprednp[i]] += 1
				if not dstr in acc_prob_by_date:
					acc_prob_by_date[dstr] = {'crc':np.zeros(2), 'fls':np.zeros(2)}
				
				pred_prob, other_prob = prob_np[i][yprednp[i]], 1 - prob_np[i][yprednp[i]]
				correct_pred = 'crc' if yprednp[i] == y_true[i] else 'fls'
				acc_prob_by_date[dstr][correct_pred] += np.array([pred_prob, other_prob ])

			# cm += metrics.confusion_matrix(y_true, y_pred, labels=range(nclasses))
			# dic_metrics = get_metrics(cm, list_metrics)

			if optimize:
				loss = criterion(out, data[1]) 
				epoch_loss += loss.item()
				loss.backward()
				optimizer.step()
				# dic_metrics['logloss'] = epoch_loss/(iteration+1)
				# dic_metrics['lr'] = optimizer.state_dict()['param_groups'][0]['lr']

			pbar.update(1)
			# pbar.set_postfix(dic_metrics)

	cm_date	= np.zeros((nclasses,nclasses), dtype=int)
	gt_by_date = []
	pred_by_date = []
	# ipdb.set_trace()
	for dstr in sorted(majority_vote.keys()):
		gt = date_true[dstr]
		mxv = max(majority_vote[dstr].values())
		try:
			pred_d = gt if majority_vote[dstr][gt] == mxv else max(majority_vote[dstr].items(),key = lambda t:t[1])[0]#.cpu().numpy()
		except:
			ipdb.set_trace()
		gt_by_date += gt,
		pred_by_date += pred_d,
	
	cm_date += metrics.confusion_matrix(gt_by_date, pred_by_date, labels=range(nclasses))
	by_date_metrics = get_metrics(cm_date, list_metrics)
	logger.info("Epoch {} - {}:".format(epoch, msg)+",".join("{}={}".format(k, v) for k,v in by_date_metrics.items()))
	if scheduler:
		scheduler.step()
	if rettype == 'mtrc_by_date':
		by_date_metrics = {k:[v] for k,v in by_date_metrics.items()}
		if optimize:
			by_date_metrics['logloss'] = [epoch_loss/(iteration+1)]
			by_date_metrics['lr'] = [optimizer.state_dict()['param_groups'][0]['lr']]
		return by_date_metrics#accum_metrics
	if rettype == 'vote':
		return majority_vote,date_true
	if rettype == 'prob':
		return majority_vote,acc_prob_by_date,date_true

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='NLP model for stock news')
	parser.add_argument('-lst','--ticker_list', nargs='+',type=str, default=["BIDU", "AAPL", "MSFT", "INTC", "QCOM", "AMD", "AMZN", "NVDA", "GOOGL"], help='ticker list that we want to crawl the prices')
	# parser.add_argument('-lst','--ticker_list', ,type=str, default='/home/zhe/project/nlp_model_bias/nlp4stock/tickers/selected_Tech', help='ticker list that we want to crawl the prices')
	parser.add_argument('-dp','--data_folder',type=str, default='/mnt/hhd/data/stock_crawl_zx/intrinio/',help='raw news path')
	parser.add_argument('-mp','--model_folder',type=str, default='/mnt/hhd/check_points',help='check point path')
	parser.add_argument('-emb','--embedding_plan',type=str, default='/mnt/hhd/data/glove_twitter_pre/glove.twitter.27B.25d.txt',help='embedding plan, glove path or train from scratch(not supported yet)')
	parser.add_argument('-ebs',"--embedding_size", type=int, default=25, help ="dimension of word embedding")
	parser.add_argument('-ld','--end_date',type=str, default='10012018',help='string of the last date that we seek for news')
	parser.add_argument('-sd','--start_date',type=str, default='10012017',help='string of the start date that we seek for news')
	parser.add_argument('-spd','--split_date',type=str, default='06012018',help='string of the split date that we use to define training and testing set')
	parser.add_argument('-cpt','--corpus_type',type=str, default='summary',help='title? body? summary?')
	parser.add_argument('-stm','--supvs_term',type=str, default='rshort',help='absolute/relative return? short/mid/long term?')
	parser.add_argument('-stp','--supvs_type',type=str, default='sign',help='supervison info type: val or sign')
	parser.add_argument('--max_sents', type=int, default=-1, help="max number of sentences per example")
	parser.add_argument('--max_words', type=int, default=-1, help="max word length")
	parser.add_argument("--solver_type", type=str, choices=['sgd', 'adam'], default='adam')
	parser.add_argument("--batch_size", type=int, default=16, help="number of example read by the gpu")
	parser.add_argument("--test_batch_size", type=int, default=None, help="batch size during prediction (equals to batch_size if none)")
	parser.add_argument("--epochs", type=int, default=200)
	parser.add_argument("--lr", type=float, default=0.0001)
	parser.add_argument("--lr_halve_interval", type=int, default=-1, help="Number of iterations before halving learning rate")
	parser.add_argument("--gamma", type=float, default=0.1, help="learning halving facor")
	parser.add_argument("--snapshot_interval", type=int, default=10, help="Save model every n epoch")
	parser.add_argument("--model_weights_path", type=str, default="")
	parser.add_argument('--gpuid', type=int, default=0, help="select gpu indice (default = -1 = no gpu used")
	parser.add_argument('--n_classes', type=int, default=2, help="number of classes")
	# parser.add_argument('--nthreads', type=int, default=4, help="number of cpu threads")

	args = parser.parse_args()

	script_exec_time = datetime.strftime(datetime.now(),'%Y%m%d_%H%M%S')
	args.model_folder = os.path.join(args.model_folder, script_exec_time)
	if not os.path.exists(args.model_folder):
		print(args.model_folder)
		os.makedirs(args.model_folder)

	logger = get_logger(logdir=args.model_folder, logname="logs.txt")
	logger.info("parameters: {}".format(vars(args)))
	id2vec, w2id = load_ebd_glove(args.embedding_plan)

	trainset = YahooFin(args.ticker_list, args.data_folder, args.start_date, args.split_date, args.corpus_type, args.supvs_term, args.supvs_type, id2vec, w2id, args.n_classes)
	testset = YahooFin(args.ticker_list, args.data_folder, args.split_date, args.end_date, args.corpus_type, args.supvs_term, args.supvs_type, id2vec, w2id, args.n_classes)
	
	logger.info("train/test set split: {}/{} (# of news)".format(len(trainset.labels), len(testset.labels)))
	# ipdb.set_trace()
	train_loader = DataLoader(trainset, batch_size=4, pin_memory=True,collate_fn=tuple_batch, shuffle=True)# 
	test_loader = DataLoader(testset, batch_size=4, pin_memory=True,collate_fn=tuple_batch,shuffle=True)#shuffle=True 
	
	# ipdb.set_trace()
	device = torch.device("cuda:{}".format(args.gpuid) if args.gpuid >= 0 else "cpu")
	n_tokens = len(w2id)
	net = create_model(args,logger,n_tokens,id2vec)
	net.to(device)

	criterion = torch.nn.CrossEntropyLoss()
	torch.nn.utils.clip_grad_norm_(net.parameters(), 1)
	# ipdb.set_trace()
	if not args.model_weights_path:
		train_base_model(args, net, w2id, train_loader, test_loader,logger)
	# evaluating
	else:
		votes = {}
		probs = {}
		ground_truth = {}
		for ticker in args.ticker_list:
			testset = YahooFin([ticker], args.data_folder, args.split_date, args.end_date, args.corpus_type, args.supvs_term, args.supvs_type, id2vec, w2id, args.n_classes)
			test_loader = DataLoader(testset, batch_size=4, pin_memory=True,collate_fn=tuple_batch,shuffle=True)
			logger.info(ticker)
			majority_vote,acc_prob_by_date,date_true = train(0,net, test_loader, device,logger, msg="testing ",list_metrics=['accuracy', 'pres_0', 'pres_1', 'recall_0', 'recall_1'], rettype='prob')
			votes[ticker] = majority_vote
			probs[ticker] = acc_prob_by_date
			ground_truth[ticker] = date_true
		with open(os.path.join(args.model_folder, 'votes'), 'wb') as wf:
			pickle.dump(votes, wf)
		with open(os.path.join(args.model_folder, 'probs'), 'wb') as wf:
			pickle.dump(probs, wf)
		with open(os.path.join(args.model_folder, 'ground_truth'), 'wb') as wf:
			pickle.dump(ground_truth, wf)