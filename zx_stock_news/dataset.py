import os
import json
import csv
import torch
import datetime as dt
from datetime import datetime
import torch.nn.functional as F
from torch.utils.data.dataset import Dataset
from data_preprocess import tokenize_corpus_and_price
import ipdb
import pickle

def tri_class_id_by_val(val, thresh1=-0.01, thresh2 = 0.01):
	if val < thresh1:
		return 0
	elif val > thresh2:
		return 2
	return 1

def bi_class_id_by_val(val, thresh = 0):
	if val > 0:
		return 0
	return 1

class YahooFin(Dataset):
	"""
	please check https://yinyangoftao@bitbucket.org/yinyangoftao/stock_news_crawler.git to crawl data
	"""
	def __init__(self, ticker_list, data_folder, start_date, end_date, corpus_type, supvs_term, supvs_type, id2vec, w2id, n_classes):
		"""
		ticker_list: a list that specifies tickers we consider for the project,1 ticker/line
		data_folder: path of the raw stock news, should have 1 corpus folder/ticker, and 1 prices folder
		corpus_type: title? body? summary?
		supvs_term: absolute/relative return? short/mid/long term?
		supvs_type: val? sign?
		"""
		self.ticker_list = ticker_list
		self.data_folder = data_folder
		self.start_date_obj = datetime.strptime(start_date,'%m%d%Y')
		# self.split_date_obj = datetime.strptime(split_date,'%m%d%Y')
		self.end_date_obj = datetime.strptime(end_date,'%m%d%Y')
		diff = self.end_date_obj - self.start_date_obj
		self.numdays = diff.days
		self.corpus_type, self.supvs_term = corpus_type, supvs_term
		sign_funcs = [bi_class_id_by_val, tri_class_id_by_val]
		sign = sign_funcs[n_classes - 2]
		self.label_trans_func = (lambda x:x) if supvs_type == 'val' else sign
		self.src_fns = []
		self.id2vec, self.w2id = id2vec, w2id
		self.n_classes = n_classes
		# Check if relevant files are in the folder_path, tokenize()
		# with open(ticker_list) as f:
		# 	for l in f:
		# 		ticker = l.strip()
		for ticker in ticker_list:
				fldr = os.path.join(data_folder,ticker)
				if os.path.exists(os.path.join(fldr)):
					token_fn = os.path.join(fldr, "yahoo_token_close_open_lbl.pkl")
					if not os.path.exists(token_fn):
						tokenize_corpus_and_price(data_folder,ticker, self.w2id, token_fn)
					self.src_fns += token_fn,
				else:
					print("ticker {} unavailable".format(ticker))
		# ipdb.set_trace()
		self.seqs, self.labels, self.dates = [],[],[]
		for fn in self.src_fns:
			with open(fn,"rb") as f:
				all_info = pickle.load(f)
				seq_tmp = all_info[corpus_type]
				sup_tmp = all_info[supvs_term]
				if len(seq_tmp) != len(sup_tmp):
					print("error in file ", fn, "! sequence length inconsistent")
					continue
				dstrs = all_info['date']
				for i, dstr in enumerate(dstrs):
					try:
						dateobj = datetime.strptime(dstr,'%m%d%Y')
						if dateobj < self.start_date_obj or dateobj >= self.end_date_obj:
							continue
						tokenized_sentences = seq_tmp[i]
						label = self.label_trans_func(sup_tmp[i])
						self.seqs += tokenized_sentences,
						self.labels += label,
						self.dates += dstr,
					except:
						pass
					# yield tokenized_sentences, label

	def __getitem__(self, index):
		return (self.seqs[index], self.labels[index], self.dates[index])

	def __len__(self):
		return len(self.labels)

# credit: nlp-benchmarks
def tuple_batch(l):
    """
    Prepare batch
    - Reorder reviews by length
    - Split reviews by sentences which are reordered by length
    - Build sentence ordering index to extract each sentences in training loop
    rtype:
    batch_t: (# of sentences, max_sentence_len)
    r_t: # of ratings
    sent_order: sent_order[i][j] is the order of the jth sentence in the ith review
    ls: ls[i] is # of words of the (globally)ith sentence
    lr: # of sentences of each review article
    review: re-ordered review
    """
    # ipdb.set_trace()
    review, rating, dates = zip(*l)
    r_t = torch.Tensor(rating).long()
    list_rev = review

    sorted_r = sorted([(len(r),r_n,r) for r_n,r in enumerate(list_rev)],reverse=True) #index by desc rev_le
    lr,r_n,ordered_list_rev = zip(*sorted_r)
    lr = list(lr)
    max_sents = lr[0]

    #reordered rating and review by # of sentences in the review(article`)
    r_t = r_t[[r_n]]
    review = [review[x] for x in r_n]

    reordered_dates = [dates[ith] for ith in r_n]

    stat =  sorted([(len(s),r_n,s_n,s) for r_n,r in enumerate(ordered_list_rev) for s_n,s in enumerate(r)],reverse=True)
    max_words = stat[0][0]

    ls = []
    batch_t = torch.zeros(len(stat),max_words).long()                         # (sents ordered by len)
    sent_order = torch.zeros(len(ordered_list_rev),max_sents).long().fill_(0) # (rev_n,sent_n)

    for i,s in enumerate(stat):
        sent_order[s[1],s[2]] = i+1 #i+1 because 0 is for empty.
        batch_t[i,0:len(s[3])] = torch.LongTensor(s[3])
        ls.append(s[0])

    #batch_t
    return batch_t, r_t, sent_order, ls, lr, review,reordered_dates