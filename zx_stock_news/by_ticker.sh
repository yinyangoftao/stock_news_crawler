#!/usr/bin/env bash
# sleep 220m

# used for evaluation
dataset="imdb"
max_sents=-1
max_words=-1
embedding_size=25
# declare -a tkr=("BIDU" "AAPL" "MSFT" "INTC" "QCOM" "AMD" "AMZN" "NVDA" "GOOGL")
# declare -a tkr=("BIDU")
model_weights_path="/media/zhe/hhd/check_points/nlp4stock/20181230_080436/model_epoch_100"
model_folder="/mnt/hhd/check_points/nlp4stock"
embedding_path="/mnt/hhd/data/glove_twitter_pre/glove.twitter.27B.25d.txt"

corpus_type="summary"
solver_type="adam"
batch_size="32"
epochs=200
lr=0.0003
lr_halve_interval=-1
gamma=0.9
snapshot_interval=10
gpuid=0
supvs_term="ashort"
n_classes=2
split_date="07012018"
end_date="10012018"

python main.py -stm ${supvs_term} \
        --model_weights_path ${model_weights_path} \
        --split_date ${split_date} \
        --end_date ${end_date} \
        --n_classes ${n_classes} \
        --model_folder ${model_folder} \
        --max_sents ${max_sents} \
        --max_words ${max_words} \
        --solver_type ${solver_type} \
        --batch_size ${batch_size} \
        --epochs ${epochs} \
        --lr ${lr} \
        --lr_halve_interval ${lr_halve_interval} \
        --gamma ${gamma} \
        --snapshot_interval ${snapshot_interval} \
        --gpuid ${gpuid} \
        --embedding_size ${embedding_size}
# done
# rm -rf "tmp"

