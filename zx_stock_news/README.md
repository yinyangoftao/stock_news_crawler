This project aims to figure out a method that can make use of the stock related news to predict stock prices and to optimize a stock portfolio.

We examined several source of stock-related news. Reuters has fine-filtered high quality news that allows date-and-ticker(company identifier)-wise queries. However, the amount of data is perhaps not enough -- even for large companies like Google and FB it's hard to guarantee 20 news articles per month. Therefore we used 3-party sources to query date-and-ticker-wise urls which is under nasdaq.com or yahoo finance domain, and then crawl news from the url. Since nasdaq.com would ban the IP, we mainly used yahoo finance news articles as source of corpus.

please check for details.

There are several source of data pipelines, such as Waynes (too many out put files and some procedures seems missing from the readme.md file, thus it's semi-automatic) and stock-net DataPipe (combined with their settings in their paper, not so general), but we don't think any that we found meet our requirements, so we decide to build our own data pipe line.

redirection data example, from other websites, which we cannot afford check one by one:http://finance.yahoo.com/r/6ad6c2b4-19bb-3b51-b3f2-3e52ed826ae3/battle-for-toshiba-a-tale-of-threats-fury-and-culture-clashes?utm_source=yahoo&utm_medium=bd&utm_campaign=headline&cmpId=yhoo.headline&yptr=yahoo&.tsrc=rss
