import os
import scrapy
import datetime as dt
from datetime import datetime
from scrapy.exceptions import CloseSpider
from scrapy.exceptions import IgnoreRequest

def daterange(end, numdays):
    """
    rtype:str, date from start to end
    """
    end_object = datetime.strptime(end,'%m%d%Y')
    date_list = [end_object - dt.timedelta(days=x) for x in range(0, numdays)]
    datestrs = list(map(lambda x:x.strftime("%m%d%Y"), date_list))
    return datestrs

class ReutersSpider(scrapy.Spider):
    name = "reuters"
    burl = "https://www.reuters.com/finance/stocks/company-news/"
    # + ticker + suffix[exchange]""
    def __init__(self, category='', **kwargs):
        super().__init__(**kwargs)  # python3
        savepath = self.savpath
        self.crawl_normal = int(self.crawl_normal)
        if not os.path.exists(self.savpath):
            os.makedirs(self.savpath)
        self.numdays = int(self.numdays)
        self.waited = 0
        self.mxwait = int(self.mxwait)

    def start_requests(self):
        # first check has news or not
        test_url = self.burl+self.ticker+self.exchange
        yield scrapy.Request(url=test_url, callback=self.request_by_date)

    def request_by_date(self, response):
        # it's not possible to check whether data is available within a range,
        # since for some company, the recent news would occupy the home page
        head = response.xpath('//div[@class="topStory"]/h2/a').extract_first()
        normal = response.xpath('//div[@class="feature"]/h2/a').extract_first()
        if not head and (not self.crawl_normal or not normal):
            print("no news for {} at all", self.ticker)
            return
        datestrs = daterange(self.enddate, self.numdays)
        for dstr in datestrs:
            dsturl = self.burl+self.ticker+self.exchange+"?date="+dstr
            yield scrapy.Request(url=dsturl, callback=self.parseStory)
            # useless to judge break here since all req are yielded together
            if self.waited > self.mxwait:
                print('close spider of ', self.ticker)
                raise CloseSpider('max wait time exceeded')
                break

    def parseStory(self, response):
        # self.log(self.ticker)
        # if self.waited > self.mxwait:
        #     raise IgnoreRequest()
        try:
            hascontent = False
            head = response.xpath('//div[@class="topStory"]/h2/a')
            body_ref = head.xpath('@href').extract_first()
            if not body_ref is None:
                body_url = "https://www.reuters.com"+body_ref
                yield scrapy.Request(url=body_url, callback=self.parseBody, meta={'news_type': "topStory"})
                hascontent = True
            if self.crawl_normal:
                normal = response.xpath('//div[@class="feature"]/h2/a')
                nml_urls = normal.xpath('@href').extract()
                for nurl in nml_urls:
                    nbody_url = "https://www.reuters.com"+nurl
                    yield scrapy.Request(url=nbody_url, callback=self.parseBody, meta={'news_type': "normal"})
                    hascontent = True
            if not hascontent:
                self.waited += 1 
            else:
                self.waited = 0 
            if self.waited > self.mxwait:
                print(response,'close spider\n\n')
                # CloseSpider not working fast, so raise it both here and in the request firing func
                raise CloseSpider('max wait time exceeded')
            print(response,"\nhas waited:{}, max wait time{}".format(self.waited,self.mxwait))
        except:
            print(response,"no body")

    def parseBody(self, response):
        try:
            article = response.xpath('//div[@class="ArticlePage_container"]/div[@class="StandardArticle_container"]')
            headline = article.xpath('//h1[@class="ArticleHeader_headline"]/text()').extract_first()
            bodyraw = article.xpath('//div[@class="StandardArticleBody_body"]/p/text()').extract()
            body = "\n".join([" ".join(br.split()) for br in bodyraw])
            rev_date = response.xpath('//meta[@name="REVISION_DATE"]/@content').extract_first()
            author = response.xpath('//p[@class="Attribution_content"]/text()').extract_first()
            news_type = response.meta.get("news_type")
            yield {
                'ticker':self.ticker,
                'title':headline,
                'time': rev_date,
                'author': author,
                'body': body,
                'news_type':news_type,
            }
        except:
            print("error with response:", response)
