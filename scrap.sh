# # run scrapy
ticker="AAPL"
enddate="11162018"
numdays="90"
savpath="/mnt/hhd/data/stock_crawl_zx/scrapy_reuters"
exchange=".O"
dstpath="${savpath}/${ticker}_${enddate}_${numdays}d.json"

python -m src.han.main  --dataset ${dataset} \
                        --data_folder ${data_folder} \
                        --model_folder ${model_folder} \
                        --max_feats ${max_feats} \
# scrapy crawl sa_AAPL 
scrapy crawl reuters -a enddate=${enddate} -a numdays=${numdays} \
					 -a exchange=${exchange} -a savpath=${savpath} \
					 -a ticker=${ticker} -o ${dstpath}