# #parse news from intrinio
import os
import json
import ipdb
import time
import requests
import datetime
import numpy as np
# import dateutil.parser
from secrets import api_key
from datetime import datetime
from bs4 import BeautifulSoup
from scrapy.http import HtmlResponse
import argparse

# r2 = requests.get(url_ps)
# d2 = r2.json()['data']
# ticker = "MSFT"
# start_date = '2018-07-01'
# end_date = '2018-10-01'
# url = "https://api.intrinio.com/news?identifier={}&start_date={}&end_date={}&api_key={}&page_size=10000".format(ticker,start_date,end_date,api_key)
# all_intrinio = requests.get(url)
# itn_json = all_intrinio.json()
# ipdb.set_trace()

def remove_whitespace(txt):
	return " ".join(" ".join([w for w in txt.split(' ') if w]).split('\t'))

def nvg_domain(url):
	return url.split('//')[-1].split('/')[0]

def crawl_passage_nasdaq(page):
	resp = HtmlResponse(url = page.url, body = page.text, encoding = page.encoding)
	txts = resp.xpath('//div[@id="articleText"]//p/text()').extract()
	news = remove_whitespace(" ".join(txts))
	return news

def crawl_passage_yhfin(page):
	# using scrapy response is much cleaner than bs
	resp = HtmlResponse(url = page.url, body = page.text, encoding = page.encoding)
	# ill:'https://finance.yahoo.com/news/ibm-largest-ever-ai-toolset-035900842.html?.tsrc=rss'
	txts = resp.xpath('//article//p/text()').extract()
	news = remove_whitespace(" ".join(txts))
	return news

def meta(ticker, start_date, end_date, metafp):
	sdobj = datetime.strptime(start_date,'%m%d%Y')
	start_Ymd = datetime.strftime(sdobj,'%Y-%m-%d')
	edobj = datetime.strptime(end_date,'%m%d%Y')
	end_Ymd = datetime.strftime(edobj,'%Y-%m-%d')
	# ipdb.set_trace()
	url = "https://api.intrinio.com/news?identifier={}&start_date={}&end_date={}&api_key={}&page_size=10000".format(ticker,start_Ymd,end_Ymd,api_key)
	all_intrinio = requests.get(url,headers={"user-agent":'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:63.0) Gecko/20100101 Firefox/63.0'})
	itn_json = all_intrinio.json()
	# itn_json = json.load(open('intrinio.json'))
	itn_news = itn_json['data']
	# ipdb.set_trace()
	# dombook = {}
	# # ipdb.set_trace()
	# # seperate by domain
	# for rec in itn_news:
	# 	dom = nvg_domain(rec['url'])
	# 	if not dom in dombook:
	# 		dombook[dom] = []
	# 	dombook[dom] += rec,

	# # filter out repeated domain
	# preferred = ['finance.yahoo.com', 'articlefeeds.nasdaq.com',]
	# dom_list = [d for d in preferred if d in dombook] + list(set(dombook.keys()) - set(preferred))
	# if 'articlefeeds.nasdaq.com' in dombook:
	# 	dom_list += 'articlefeeds.nasdaq.com',
	# dom_list += list(set(dombook.keys()) - set(['articlefeeds.nasdaq.com']))
	# print("domain names:", dom_list)
	# visited = set()
	# # filter out repeat and store into itn_news(erase the orig list)
	# itn_news = []
	# # ipdb.set_trace()
	# for dom in dom_list:
	# 	if not dom in funcs:
	# 		print('new domain: jump\n\n\n', dom)
	# 		continue
	# 	for rec in dombook[dom]:
	# 		if rec['title'] in visited:
	# 			continue
	# 		itn_news += rec,
	# 		visited.add(rec['title'])
	# print('saving non-repeat meta info into', metafp)
	# ipdb.set_trace()
	json.dump(itn_news, open(metafp,"w"))
	return itn_news

def crawl_ticker_daterange(savepath, ticker = "MSFT", start_date = '07012018', end_date = '10012018', repeat = 2):
	subpath = os.path.join(savepath, ticker)
	if not os.path.exists(subpath):
		os.makedirs(subpath)

	metafn = "meta_"+start_date+"_to_"+end_date+".json"
	metafp = os.path.join(subpath,metafn)
	detailfp = os.path.join(subpath,"detail_"+start_date+"_to_"+end_date+".json")

	if not os.path.exists(metafp):
		itn_news = meta(ticker, start_date, end_date, metafp)
	else:
		itn_news = json.load(open(metafp))

	print('in total {} unique news'.format(len(itn_news)))

	# ipdb.set_trace()
	# use tmp = [rec for d in qcom for rec in qcom[d] if not 'yahoo' in rec['url']] to select only nasdaq data
	visited = set()
	datebook = {}
	for i,rec in enumerate(itn_news):
		try:
			dom, title = nvg_domain(rec['url']), rec['title']
			if not dom in funcs or title in visited:
				continue
			time.sleep(np.random.poisson(1))
			for rpt in range(repeat):
				try:
					page = requests.get(rec['url'])
					if page.status_code == 403:
						sleep = 10+10*(rpt+1)
						print('retry for 403:',rec['url'],', sleep for ', sleep, 's')
						time.sleep(sleep)
						continue
					elif page.status_code != 200:
						print('skip on getting http error:',rec['url'],'\n,status:',page.status_code)
						break
					news = funcs[dom](page)
					rec['body'] = news
					dateobj = datetime.strptime(rec['publication_date'][:19], "%Y-%m-%dT%H:%M:%S")
					datestr = datetime.strftime(dateobj,'%m%d%Y')
					if not datestr in datebook:
						datebook[datestr] = []
					datebook[datestr] += rec,
					visited.add(title)
					break
				except:
					sleep = 10+10*(rpt+1)
					print('http error:',rec['url'],'\n, sleep for ', sleep, 's')
					time.sleep(sleep)
			# if len(datebook.keys()) > 360:
				# break
			print("has gained news in {} days".format(len(datebook.keys())))
			if i%50 == 0:
				print("temporarily dump to ", detailfp)
				json.dump(datebook, open(detailfp,"w"))
		except:
			print("failed on ", rec)
	json.dump(datebook, open(detailfp,"w"))
	return datebook

funcs = {'finance.yahoo.com':crawl_passage_yhfin}#,'articlefeeds.nasdaq.com':crawl_passage_nasdaq,  'uk.finance.yahoo.com':crawl_passage_yhfin, 'www.yahoo.com':crawl_passage_yhfin, 'uk.news.yahoo.com':crawl_passage_yhfin}
if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='finance data crawler.')
	parser.add_argument('-tl','--ticker_list', type=str, default='./tickers/Technology', help='ticker list that we want to crawl the prices')
	parser.add_argument('-sp','--savepath',type=str, default='/mnt/hhd/data/stock_crawl_zx/intrinio',help='dst json file path')
	parser.add_argument('-ld','--end_date',type=str, default='10012018',help='string of the last date that we seek for news')
	parser.add_argument('-sd','--start_date',type=str, default='10012017',help='string of the start date that we seek for news')
	parser.add_argument('-rp', '--repeat', type = int, default=3,help='number of repeat if request fails')
	args = parser.parse_args()
	# crawl_ticker_daterange('/mnt/hhd/data/stock_crawl_zx/intrinio')
	with open(args.ticker_list) as tf:
		for ln in tf:
			ticker = ln.strip()
			print("process ", ticker)
			crawl_ticker_daterange(args.savepath, ticker, args.start_date, args.end_date, args.repeat)
			# time.sleep(70)