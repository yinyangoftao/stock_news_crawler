import json
import os
# from collections import Counter
from intrinio import nvg_domain
import datetime as dt
from datetime import datetime
import numpy as np
import ipdb
import matplotlib.pyplot as plt

def draw_fig(stats,sav_path):
	
	L = set(map(len, stats.values()))
	assert len(L) == 1, "need same # of days for all tickers"
	L = L.pop()
	print("stats for ",L," days")
	x = list(range(L))
	fig, ax = plt.subplots()
	for ky in stats:
		fig_tmp, ax_tmp = plt.subplots()
		y = stats[ky]
		ax_tmp.plot(x, y, label=ky)
		fig_tmp.savefig(sav_path+ky)
		plt.close(fig_tmp)
		ax.plot(x, y, label=ky)
	ax.legend()
	# plt.show()
	plt.savefig(sav_path)

def days_between(start, end):
    """
    rtype:str, date from start to end
    """
    start_object = datetime.strptime(start,'%m%d%Y')
    end_object = datetime.strptime(end,'%m%d%Y')
    dates = end_object - start_object
    days = dates.days
    return days

def yahoo_news_stats(path, lst_path, std = "10012017", edd="10012018"):
	yahoo_news_cnt = {}
	Ndays = days_between(std, edd)+1
	# ipdb.set_trace()
	with open(lst_path) as f:
		for l in f:
			ticker = l.strip()
			# if not ticker in yahoo_news_cnt:
			yahoo_news_cnt[ticker] = [0]*Ndays
			fldr = os.path.join(path,ticker)
			print(fldr)
			prefixed = [fn for fn in os.listdir(fldr) if fn.startswith("detail")]
			for fl in prefixed:
				data = json.load(open(os.path.join(fldr,fl)))
				for datestr, val in data.items():
					# need to further deal with repeating ends
					try:
						date_id = days_between(std, datestr)
						for rec in val:
							if nvg_domain(rec['url']) == 'finance.yahoo.com':
								yahoo_news_cnt[ticker][date_id] += 1
					except:
						# ipdb.set_trace()
						print(date_id, datestr)
	return yahoo_news_cnt


if __name__ == "__main__":
	path = "/mnt/hhd/data/stock_crawl_zx/intrinio/"
	lst_path = "/home/zhe/project/nlp_model_bias/data_crawl_zx/tickers/selected_Tech"
	stats = yahoo_news_stats(path, lst_path)
	fig = draw_fig(stats, lst_path+"_stats")
