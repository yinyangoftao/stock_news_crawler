enddate="10012018"
numdays="90"
savpath="/mnt/hhd/data/stock_crawl_zx/scrapy_reuters"
exchange=".O"
mxwait="45"
crawl_normal="1"
declare -a ind=("Technology")
#"Energy" 
declare -a exch=(".O" ".N" ".A")
## now loop through the above array

IFS=$'\n'       # make newlines the only separator
set -f          # disable globbing

for j in "${ind[@]}"; do
	for i in $(cat < "tickers/${j}"); do
		for e in "${exch[@]}"; do
			dstpath="${savpath}/${j}${e}/${i}_${enddate}_${numdays}d.json"
			echo "${dstpath}"
			scrapy crawl reuters -a enddate=${enddate} -a numdays=${numdays} \
							 -a exchange=${e} -a savpath=${savpath} \
							 -a ticker=${i} -a mxwait=${mxwait} \
							 -a crawl_normal=${crawl_normal} -o ${dstpath}
		done
	done
done